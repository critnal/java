package heroAnswers;

import heroQuestions.SuperPower;

public class EnhancedHuman extends SuperHero {
	
	private SuperPower[] acquiredPowers;

	public EnhancedHuman(String normalIdentity, String enhancedIdentity, SuperPower[] acquiredPowers) {
		super(normalIdentity, enhancedIdentity);
		// TODO Auto-generated constructor stub
		
		this.acquiredPowers = acquiredPowers;
	}

	@Override
	public boolean hasPower(SuperPower queriedPower) {
		// TODO Auto-generated method stub
		for (SuperPower power : acquiredPowers) {
			if (power.name() == queriedPower.name()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int totalPower() {
		// TODO Auto-generated method stub
		int total = 0;
		if (currentIdentity == trueIdentity) {
			return 0;
		}
		for (SuperPower power : acquiredPowers) {
			total += power.getValue();
		}
		
		return total;
	}

}
