package heroAnswers;

public class SecretAgent implements Hero {
	
	protected String trueName, codeName, currentIdentity, gadget;

	public SecretAgent(String trueName, String codeName, String gadget) {
		this.trueName = trueName;
		this.codeName = codeName;
		this.gadget = gadget;
		this.currentIdentity = trueName;
	}

	@Override
	public String currentIdentity() {
		return currentIdentity;
	}

	@Override
	public void switchIdentity() {
		currentIdentity = codeName;
	}
	
	public String getGadget() {
		return gadget;
	}

}
