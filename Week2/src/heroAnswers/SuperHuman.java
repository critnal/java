package heroAnswers;

import java.util.ArrayList;

import heroQuestions.SuperPower;

public class SuperHuman extends SuperHero {
	
	private ArrayList<SuperPower> innatePowers;

	public SuperHuman(String realName, String secretIdentity, SuperPower[] innatePowers) {
		super(realName, secretIdentity);
		this.innatePowers = new ArrayList<SuperPower>();
		for (SuperPower power : innatePowers) {
			this.innatePowers.add(power);
		}
	}

	@Override
	public boolean hasPower(SuperPower queriedPower) {
		return (innatePowers.contains(queriedPower) ? true : false);
	}

	@Override
	public int totalPower() {
		int total = 0;
		for (SuperPower power : innatePowers) {
			total += power.getValue();
		}
		return total;
	}
	
	public void acquirePowers(SuperPower[] newPowers) {
		for (SuperPower power : newPowers) {
			this.innatePowers.add(power);
		}
	}
	
	public void losePowers(SuperPower[] oldPowers) {
		for (SuperPower power : oldPowers) {
			this.innatePowers.remove(power);
		}
	}

}
