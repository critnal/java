package footyAnswers;

public class FootyScore {
	
	private int score, goals, behinds;
	
	public int getPoints() {
		return score;
	}
	
	public void kickGoal() {
		goals++;
		score += 6;
	}
	
	public void kickBehind() {
		behinds++;
		score++;
	}
	
	public String sayScore() {
		String say = "";		
		say = String.format("%d, %d, %d", goals, behinds, score);		
		return say;
	}
	
	public boolean inFrontOf(FootyScore inputScore) {
		if (inputScore.getPoints() > score) {
			return true;
		}
		return false;
	}
}
