package asgn1Solution;

import java.util.Random;

import asgn1Question.HiringException;
import asgn1Question.IApplicant;
import asgn1Question.IHiringGame;

public class HiringGame implements IHiringGame {
	
	private Applicant[] applicants;
	private int currentApplicantIndex;
	private boolean isAccepted;
	
	public HiringGame() {
		
	}

	@Override
	public void newGame(int maxApplicants, Random random) throws HiringException {
		applicants = new Applicant[maxApplicants];
		for (int i = 0; i < maxApplicants; i++) {
			applicants[i] = new Applicant(i, random.nextDouble());
		}
		currentApplicantIndex = 0;
		isAccepted = false;
	}

	@Override
	public IApplicant getNextApplicant() throws HiringException {
		currentApplicantIndex++;
		return applicants[currentApplicantIndex];
	}

	@Override
	public boolean isAccepted() throws HiringException {
		return isAccepted;
	}

	@Override
	public void acceptApplicant() throws HiringException {
		isAccepted = true;
	}

	@Override
	public boolean isBestApplicant() throws HiringException {
		double acceptedApplicantScore = applicants[currentApplicantIndex].getQualityScore();
		double bestApplicantScore = getBestApplicant().getQualityScore();
		if (acceptedApplicantScore == bestApplicantScore) {
			return true;
		}
		return false;
	}

	@Override
	public IApplicant getBestApplicant() throws HiringException {
		double highestScore = 0;
		int bestApplicant = 0;
		for (int i = 0; i < applicants.length; i++) {
			if (applicants[i].getQualityScore() >= highestScore) {
				highestScore = applicants[i].getQualityScore();
				bestApplicant = i;
			}
		}
		return applicants[bestApplicant];
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
