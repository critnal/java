package asgn1Solution;

import asgn1Question.IApplicant;

public class Applicant implements IApplicant {
	
	private int id;
	private double qualityScore;
	
	public Applicant(int id, double qualityScore) throws IllegalArgumentException {
		setId(id);
		setQualityScore(qualityScore);
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int appId) {
		id = appId;
	}

	@Override
	public double getQualityScore() {
		return qualityScore;
	}

	@Override
	public void setQualityScore(double qualityScore)
			throws IllegalArgumentException {
		this.qualityScore = qualityScore;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
